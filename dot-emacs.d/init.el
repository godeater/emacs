;; Turn off some bits of the mouse interface which aren't terribly useful

(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; Turn off the splash screen / startup message
(setq inhibit-startup-message t)

;; Change default paths
(setq site-lisp-dir
      (expand-file-name "site-lisp" user-emacs-directory))
(setq settings-dir
      (expand-file-name "settings" user-emacs-directory))

;; Set load path
(add-to-list 'load-path settings-dir)
(add-to-list 'load-path site-lisp-dir)

;; Make emacs put custom-settings stuff in a separate file so they
;; don't get mixed in with manually set stuff in init.el
(setq custom-file
      (expand-file-name "custom.el" user-emacs-directory))
(if (file-exists-p custom-file)
    (load custom-file))

;; Settings for currently logged in user
(setq user-settings-dir
      (concat user-emacs-directory "users/" user-login-name))
(add-to-list 'load-path user-settings-dir)

;; Write backup files to their own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
		 (concat user-emacs-directory "backups")))))

(cond ((eq system-type 'windows-nt)
			 ;; Override find program with specific path to a binary
			 (setq find-program "c:/Progra~1/Git/usr/bin/find.exe")
			 ;; Override grep program with specific path to a binary
			 (setq grep-program "c:/Progra~1/Git/usr/bin/grep.exe")
			 ))

;; The check for proxy requirements has to happen early, otherwise
;; the bootstrap of the straight package manager will fail
(require 'setup-proxy)

;; turn off built in package manager
(setq package-enable-at-startup nil)

;; "straight" is an alternative package system for emacs
;; which tries to solve a lot of the problems which
;; package.el and melpa have
(require 'setup-straight)

;; Now use straight to actually install / maintain packages
(require 'setup-packageinstalls)

;; Now settings for discrete packages
(require 'setup-company)
(require 'setup-ctags)
(require 'setup-ein)
(require 'setup-go)
(require 'setup-javascript)
(require 'setup-org)
(require 'setup-origami)
(require 'setup-powershell)
(require 'setup-rustic)
(require 'setup-xclip)

;; Finally, my own customisations
(require 'setup-customfunctions)
(require 'setup-customhooks)
(require 'setup-keybinds)

(require 'appearance)
(require 'personal-settings)

;; Start emacs server to handle emacsclient requests
(server-start)
