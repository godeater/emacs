;; Turn off some bits of the mouse interface which I don't use much

(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; Turn off the splash screen thingy
(setq inhibit-startup-message t)

;; Change default paths
(setq site-lisp-dir
	  (expand-file-name "site-lisp" user-emacs-directory))
(setq settings-dir
	  (expand-file-name "settings" user-emacs-directory))

;; Set load path
(add-to-list 'load-path settings-dir)
(add-to-list 'load-path site-lisp-dir)

;; Make emacs store customized stuff in a seperate file
(setq custom-file
	  (expand-file-name "custom.el" user-emacs-directory))
(if (file-exists-p custom-file)
	(load custom-file))

;; Settings for currently logged in user ?!?
(setq user-settings-dir
	  (concat user-emacs-directory "users/" user-login-name))
(add-to-list 'load-path user-settings-dir)

;; Write backup files to their own directory
(setq backup-directory-alist
	  `(("." . ,(expand-file-name
				 (concat user-emacs-directory "backups")))))

(defvar bootstrap-version)
(let ((bootstrap-file
	   (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	  (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
	(with-current-buffer
		(url-retrieve-synchronously
		 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
		 'silent 'inhibit-cookies)
	  (goto-char (point-max))
	  (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(require 'setup-straight)
(require 'setup-org)
(require 'appearance)
