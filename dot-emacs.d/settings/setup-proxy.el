;; Check to see if we're running on a host which needs a proxy
;; to access the internet

;; Hosts which don't:
;;  aphrodite
;;  athena
;;  atlas
;;
;; Hosts which do:
;;  lonmw84148
(if (string-equal system-name "LONMW84148")
    (setq url-proxy-services '(
			       ("http" . "127.0.0.1:3128")
			       ("https" . "127.0.0.1:3128")
			       )))

(provide 'setup-proxy)
