(require 'lsp-mode)
(add-hook 'prog-mode-hook #'lsp)

(provide 'setup-lsp)
