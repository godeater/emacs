;; Install my favourite theme
;;(straight-use-package 'tangotango-theme)
(straight-use-package
 '(tangotango-theme :type git :host gitlab :repo "godeater/color-theme-tangotango"))
;; Selection of doom-themes
(straight-use-package 'doom-themes)
;; and the powerline theme
(straight-use-package 'powerline)
;; package for fonts with loads of useful icons
(straight-use-package 'all-the-icons)
;; doom modeline
(straight-use-package 'doom-modeline)

(require 'frame-cmds)

;; Set frame to maximised
;;(add-to-list 'default-frame-alist '(fullscreen . maximized))
(add-to-list 'default-frame-alist '(left . 0))
(add-to-list 'default-frame-alist '(top . 0))
(add-to-list 'default-frame-alist '(height . 39))
(add-to-list 'default-frame-alist '(width . 158))

;; Not stricly appearance related, but it means characters will display properly
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; Also not really appearance related - but tabs are common everywhere
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

(setq font-lock-maximum-decoration t
      color-theme-is-global t
      truncate-partial-width-windows nil)

;; Don't beep. Don't visible bell. Just blink the modeline on errors

(setq visible-bell nil)
(setq ring-bell-function (lambda ()
			   (invert-face 'mode-line)
			   (run-with-timer 0.05 nil 'invert-face 'mode-line)))

;; Highlight current line
;;(global-hl-line-mode 1)

;; Set custom theme path
(setq custom-theme-directory (concat user-emacs-directory "themes"))
(if (not(file-directory-p custom-theme-directory))
    (make-directory custom-theme-directory))

(dolist
    (path (directory-files custom-theme-directory t "\\w+"))
  (when (file-directory-p path)
    (add-to-list 'custom-theme-load-path path)))

;; Default theme
(defun use-presentation-theme ()
  (interactive)
  (when (boundp 'bryan/presentation-font)
    (set-face-attribute 'default nil :font bryan/presentation-font)))

(defun use-default-theme ()
  (interactive)
  (require 'doom-themes)
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-one)
  (doom-themes-neotree-config)
  (doom-themes-org-config)
  (when (fboundp 'neotree-toggle)
    (doom-themes-neotree-config))
  (when (boundp 'bryan/default-font)
    (set-face-attribute 'default nil :font bryan/default-font)))

(defun toggle-presentation-mode ()
  (interactive)
  (if (string= (frame-parameter nil 'font) bryan/default-font)
      (use-presentation-theme)
    (use-default-theme)))

(global-set-key (kbd "C-<f9>") 'toggle-presentation-mode)

(use-default-theme)

;; Powerline sub-theme settings
;;(require 'powerline)
;;(powerline-default-theme)
;;(set-face-attribute 'mode-line nil :box nil :font "PragmataPro Mono-10")
;;(setq powerline-color1 "grey22")
;;(setq powerline-color2 "grey40")
;;(setq powerline-default-seperator 'curve)
;;(setq powerline-arrove-shape 'curve)
;;(setq powerline-default-separator-dir '(right . left))

;; Doom modeline sub theme settings
(require 'doom-modeline)
(doom-modeline-mode 1)

;; Don't defer screen updates when performing operations
(setq redisplay-dont-pause t)

;; org-mode colours
;;(setq org-todo-keyword-faces
;;      '(
;;	("INPR" . (:foreground "yellow" :weight bold))
;;	("DONE" . (:foreground "green" :weight bold))
;;	("IMPEDED" . (:foreground "red" :weight bold))
;;	))
(setq org-todo-keyword-faces
      (quote (
	      ("TODO" :foreground "tomato" :weight bold)
	      ("NEXT" :foreground "blue" :weight bold)
	      ("WAITING" :foreground "orchid" :weight bold)
	      ("IN-PROGRESS" :foreground "salmon" :weight bold)
	      ("ON-HOLD" :foreground "RoyalBlue1" :weight bold)
	      ("MEETING" :foreground "forest green" :weight bold)
	      ("PHONE" :foreground "forest green" :weight bold)
	      ("DONE" :foreground "forest green" :weight bold)
	      ("CANCELLED" :foreground "forest green" :weight bold))))

;; Highlight matching parentheses when the point is on them
(show-paren-mode 1)
;; Also turn on rainbow delimiters
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode 1)
  (blink-cursor-mode -1))



(provide 'appearance)
