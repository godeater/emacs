(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "<f7>") 'switch-to-minibuffer-window)
(global-set-key (kbd "<f8>") 'neotree-toggle)
(global-set-key (kbd "M-Q") 'reindent-whole-buffer)
(global-set-key (kbd "<scroll>") (lambda () (interactive) ()))

(provide 'setup-keybinds)
