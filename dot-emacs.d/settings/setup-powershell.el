;; Minimal config for powershell-mode
(modify-coding-system-alist 'file "\\.psm?1\\'" 'utf-8-with-signature-dos)
(add-to-list 'auto-mode-alist '("\\.psm?1\\'" . powershell-mode))
;; Enable auto-completion of all default Powershell functions and cmdlets
(add-to-list 'tags-table-list (expand-file-name "tag-files/powershell" user-emacs-directory))

(provide 'setup-powershell)
