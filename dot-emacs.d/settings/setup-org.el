(defun myorg-update-parent-cookie ()
  (when (equal major-mode 'org-mode)
    (save-excursion
      (ignore-errors
	(org-back-to-heading)
	(org-update-parent-todo-statistics)))))

(defadvice org-kill-line (after fix-cookies activate)
  (myorg-update-parent-cookie))

(defadvice kill-whole-line (after fix-cookies activate)
  (myorg-update-parent-cookie))

(setq org-directory "~/Org-Mode")

(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq org-agenda-files (list
			(concat org-directory "/gtd.org")
			(concat org-directory "/office.org")
			(concat org-directory "/work.org")
			(concat org-directory "/home.org")
			))
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "WAITING(w@/!)" "IN-PROGRESS(p@/!)" "ON-HOLD(h@/!)" "PHONE" "MEETING" "|" "DONE(d)" "CANCELLED(c@/!"))))

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline (lambda () (concat org-directory "/gtd.org")) "Tasks")
	 "* TODO %?\n %i\n %a")
	("j" "Journal" entry (file+olp+datetree (lambda () (concat org-directory "/journal.org")))
	 "* %?\n Entered on %U\n %i\n %a")
	("o" "Outlook Email" entry (file (lambda () (concat org-directory "/work.org")))
	 "* TODO %c %?
          %i
          %U" :clock-in t :clock-resume t)))

;; Stop spaces being replaced with tabs on export
(setq org-src-preserve-indentation nil
      org-edit-src-content-indentation 0)

;; Enable ditaa diagrams to work
(require 'ob-ditaa)

;; Custom function to allow yaml source blocks
(defun org-babel-execute:yaml (body params) body)

;; Always allow babel code execution for ditaa and yaml
(defun my-org-confirm-babel-evaluate (lang body)
  (and (not (string= lang "ditaa"))
       (not (string= lang "yaml")))
       )        ;don't ask for ditaa or yaml
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

;; JIRA
;; Potential JQL filters
;; My tickets, current sprint = 'assignee = currentUser() and Sprint in openSprints()'
(require 'org-jira)
;;(setq org-jira-working-dir (lambda () (concat org-directory "/org-jira")))
;;(setq org-jira-default-jql "assignee = currentUser() and Sprint in openSprints() ORDER BY priority DESC, created ASC")
(setq jiralib-url "https://super-retail-group.atlassian.net")

(define-key global-map (kbd "C-<f6>") 'org-capture)

(provide 'setup-org)
