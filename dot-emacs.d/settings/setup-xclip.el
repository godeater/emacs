;; Set a hook to modify the xclip-mode activation
;; whenever a new frame is created.
(add-hook 'after-make-frame-functions
  (lambda ()
    (if (not (window-system))
	(xclip-mode 1)
      )))

;; Run the same check when emacs starts too - since the initial frame
;; is created before the hook above is installed
(if (not (window-system))
    (xclip-mode 1))


(provide 'setup-xclip)
