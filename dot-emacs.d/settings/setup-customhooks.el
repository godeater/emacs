(defun disable-tab-indents ()
  "Turn off tab indents when using picture mode"
  (setq indent-tabs-mode nil))

(add-hook 'picture-mode-hook 'disable-tab-indents)

(provide 'setup-customhooks)
