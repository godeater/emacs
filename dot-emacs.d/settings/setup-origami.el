(defvar origami-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<C-S-return>") #'origami-reset)
    (define-key map (kbd "<C-M-return>") #'origami-open-all-nodes)
    (define-key map (kbd "<C-return>") #'origami-recursively-toggle-node)
    map))

(add-hook 'prog-mode-hook
	  (lambda () (origami-mode)))

(provide 'setup-origami)
