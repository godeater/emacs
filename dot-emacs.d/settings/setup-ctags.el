;; Set up to use universal ctags (https://ctags.io/)
(if (string-equal system-type "windows-nt")
    (setq path-to-ctags "c:/dev/Downloads/Development/ctags-2019-01-08_b60a7b92-x64/ctags.exe"))

(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name)))
  )

;; Stop nagging me to add things to the TAGS table!
(setq tags-add-tables nil)

(provide 'setup-ctags)
