;; Now actually install org-mode
(straight-use-package 'org)

;; Also install org-contrib
(straight-use-package 'org-contrib)

;; Additionally, install org-d20
(straight-use-package 'org-d20)

;; Frame-cmds for working with emacs window sizes
(straight-use-package 'frame-cmds)

;; Blank mode to display whitespace when required
(straight-use-package 'blank-mode)

;; Dependencies for origami mode
(straight-use-package 'dash)
(straight-use-package 's)

;; Origami folding mode
(straight-use-package 'origami)

;; Icicles (easy emoji input)
(straight-use-package 'icicles)

;; Setup XClip
(straight-use-package 'xclip)

;; Also install org-jira
(straight-use-package 'org-jira)

;; Install ob-pwsh to allow executing pwsh scripts
(straight-use-package
 '(ob-pwsh :type git :host github :repo "togakangaroo/ob-pwsh"))

;; Install neotree
(straight-use-package
 '(neotree :type git :host github :repo "jaypei/emacs-neotree"))

;; Install rainbow delimiters
(straight-use-package 'rainbow-delimiters)

;; Dumb-Jump
(straight-use-package
 '(dumb-jump :type git :host github :repo "jacktasia/dumb-jump"))

;; HMTLize
(straight-use-package
 '(htmlize :type git :host github :repo "hniksic/emacs-htmlize"))

;; XML-RPC
(straight-use-package
 '(xml-rpc :type git :host github :repo "hexmode/xml-rpc-el"))

;; Confluence
(straight-use-package 'confluence)

;; AutoHotKey Mode
;;(straight-use-package 'xahk-mode)

;; Install magit
(straight-use-package 'magit)

;; Install flycheck
(straight-use-package 'flycheck)

;; Install lsp mode (Language Server Protocol)
(straight-use-package 'lsp-mode)
;; '(lsp-mode :type git :host github :repo "emacs-lsp/lsp-mode"))

;; UI extensions for lsp mode
(straight-use-package
 '(lsp-ui :type git :host github :repo "emacs-lsp/lsp-ui"))

;; Install mode for golang
(straight-use-package 'go-mode)

;; Rust mode
;; (straight-use-package 'rust-mode)
;; rustic (fork of rust-mode)
(straight-use-package 'rustic)
;; racer for rust
;;(straight-use-package 'racer)

;; Install powershell-mode
(straight-use-package
 '(powershell-mode :type git :host github :repo "jschaf/powershell.el"))

;; JSX (React) Mode
(straight-use-package 'rjsx-mode)

;; Mode for Arch Linux PKGBUILD files
(straight-use-package 'pkgbuild-mode)

;; Mode for Hashicorp HCL
(straight-use-package 'hcl-mode)

;; YAML mode
(straight-use-package 'yaml-mode)

;; Emacs IPython Notebook and dependencies
(straight-use-package 'ein)

;; Install company-mode
(straight-use-package
 '(company-mode :type git :host github :repo "company-mode/company-mode"))

;; Install company mode extension for lsp-mode
(straight-use-package
 '(company-lsp :type git :host github :repo "tigersoldier/company-lsp"))

;; Language specific company extensions
(straight-use-package 'company-go)

;; yasnippet
(straight-use-package 'yasnippet)

;; emacs-crossword
(straight-use-package
 '(emacs-crossword :type git :host github :repo "Boruch-Baum/emacs-crossword" :branch main))

;; edit-server, required for edit in emacs firefox extension
(straight-use-package 'edit-server)

(provide 'setup-packageinstalls)
