(defgroup scratch-message nil
  "Working with the message in your scratch buffer"
  :group 'environment)

(defcustom scratch-message-function 'scratch-message-function-default
  "Function called by `scratch-message-trigger-message' that
should generate a message and insert it by calling 
`scratch-message-insert'."
  :group 'scratch-message
  :type 'function)

(defcustom scratch-message-interval 10
  "Time in seconds to wait between two messages."
  :group 'scratch-message
  :type 'number)

(defcustom scratch-message-invisible t
  "If non-nil, do not change message if the scratch buffer is
visible."
  :group 'scratch-message
  :type 'boolean)

(defcustom scratch-message-retry 3
  "Time in seconds to wait before trying to redisplay."
  :group 'scratch-message
  :type 'number)

(defcustom scratch-message-quotes
  '("You can do anything, but not everything."
    "Perfection is achieved, not when there is nothing more to add, but where there is
nothing left to take away.")
  "Some quotes taken from places and probably out of context."
  :group 'scratch-message
  :type '(repeat string))

;; Internal variables
(defvar scratch-message-timer nil)
(defvar scratch-message-beg-marker (make-marker))
(defvar scratch-message-end-marker (make-marker))
(defvar scratch-message-timestamp nil)

(defun scratch-message-function-default ()
  "Default function called to display a new message."
  (scratch-message-insert (nth (random (length scratch-message-quotes)) scratch-message-quotes)))

(defun scratch-message-fortune ()
  "Return a fortune as a string.

You need to properly set the value of `fortune-file' first. This
function can be used in place of
`scratch-message-function-default'."
  (require 'fortune)
  (fortune-in-buffer t)
  (scratch-message-insert
   (with-current-buffer fortune-buffer-name
     (buffer-string))))

(defun scratch-message-insert (message)
  "Replace or insert the message MESSAGE in the scratch buffer.

If there is no previous message, insert MESSAGE at the end of the 
buffer, make sure we are on a beginning of a line and add three
newlines at the end of the message."
  (if (get-buffer "*scratch*")
      (with-current-buffer "*scratch*"
	(let ((bm (buffer-modified-p)))
	  (if (and (marker-position scratch-message-beg-marker)
		   (marker-position scratch-message-end-marker))
	      (delete-region scratch-message-beg-marker scratch-message-end-marker))
	  (save-excursion
	    (if (marker-position scratch-message-beg-marker)
		(goto-char (marker-position scratch-message-beg-marker))
	      (goto-char (point-max))
	      (or (bolp) (insert "\n"))
	      (save-excursion (insert "\n\n\n")))
	    (set-marker scratch-message-beg-marker (point))
	    (insert message)
	    (set-marker scratch-message-end-marker (point))
	    (let ((comment-start (or comment-start " ")))
	      (comment-region scratch-message-beg-marker
			      scratch-message-end-marker)))
	  (set-buffer-modified-p bm)))
    (error "No scratch buffer")))

;;;###autoload
(define-minor-mode scratch-message-mode
  "Minor mode to insert message in your scratch buffer."
  :lighter ""
  :global t
  (if scratch-message-mode
      (unless (timerp scratch-message-timer)
	(setq scratch-message-timer (run-with-timer 30 nil 'scratch-message-trigger-message)))
    (if (timerp scratch-message-timer) (cancel-timer scratch-message-timer))
    (setq scratch-message-timer nil)))

(provide 'scratch-message)
