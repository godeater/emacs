#+STARTUP: showall
* Questions
  1. What will the access model to Secret-Service look like? \\
     Currently it only allows access if the JWT it is presented the admin claim in it.
  2. What secrets engines will microservices want to use? Just kv, or others?
  3. Should Secret-Service manage the access to given secrets \\
     within Vault, or should we be setting up individual roles in Vault?
  4. What does this mean for app-seg of Vault?
  5. If connectivity to Vault / Secret Service is required from \\
     AWS PCF, what firewall changes will be required?
  6. What secrets engines does PCF want to use?
  7. Should TFE really be a special case that goes around Secret-Service? \\
     As soon as we allow that, we might as well bin the secret-service \\
     entirely, as we've then already been locked in to using Vault by \\
     TFE, so other apps might as well use it direct too.
  8. What requirement is there for LDAP auth to Vault beyond BreakGlass access?





* We're splitting into two feature teams.
* My team handles 
  - Access Control
  - Configuration Managmeent
  - Pipeline Orchestration
  - Secret Management
* 
